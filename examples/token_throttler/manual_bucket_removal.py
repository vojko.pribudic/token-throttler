from token_throttler import TokenBucket, TokenThrottler
from token_throttler.storage import RuntimeStorage

throttler: TokenThrottler = TokenThrottler(1, RuntimeStorage())
throttler.add_bucket("hello_world", TokenBucket(10, 10))
throttler.add_bucket("hello_world", TokenBucket(30, 20))


def hello_world() -> None:
    print("Hello World")


for i in range(10):
    throttler.consume("hello_world")
    hello_world()

# remove bucket by bucket_key (str representation of replenish_time param)
throttler.remove_bucket("hello_world", "10")

# bucket one (limit of 10 tokens) is removed, no throttle limit hit
if throttler.consume("hello_world"):
    hello_world()
else:
    print(
        f"bucket_one ran out of tokens, retry in: {throttler.wait_time('hello_world')}"
    )
