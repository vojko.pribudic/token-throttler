import asyncio
from time import sleep

from fakeredis import FakeRedis

from token_throttler import TokenBucket, TokenThrottler, TokenThrottlerException
from token_throttler.storage.redis import RedisStorage

# Replace FakeRedis with Redis instance :)
throttler: TokenThrottler = TokenThrottler(
    1, RedisStorage(redis=FakeRedis(), delimiter="||")
)
throttler.add_bucket("hello_world", TokenBucket(5, 10))
throttler.add_bucket("hello_world", TokenBucket(30, 20))


@throttler.enable("hello_world")
async def hello_world() -> None:
    print("Hello World")


for i in range(10):
    asyncio.run(hello_world())

try:
    asyncio.run(hello_world())
except TokenThrottlerException:
    print(
        f"bucket_one ran out of tokens, retry in: {throttler.wait_time('hello_world')}"
    )

sleep(5)
asyncio.run(hello_world())
