from functools import wraps
from typing import Any, Callable

from token_throttler import TokenBucket, TokenThrottler, TokenThrottlerException
from token_throttler.storage import RuntimeStorage

throttler: TokenThrottler = TokenThrottler(1, RuntimeStorage())
throttler.add_bucket("hello_world", TokenBucket(10, 10))
throttler.add_bucket("hello_world_raise", TokenBucket(10, 10))


def custom_decorator(identifier: str, raise_exc: bool = False) -> Any:
    def wrapper(fn: Callable):
        @wraps(fn)
        def inner(*args, **kwargs):
            if throttler.consume(identifier):
                return fn(*args, **kwargs)
            else:
                if raise_exc:
                    raise TokenThrottlerException
                else:
                    print(
                        f"Rate limit exceeded, retry in: {throttler.wait_time(identifier)}"
                    )

        return inner

    return wrapper


@custom_decorator("hello_world")
def hello_world() -> None:
    print("Hello World")


# Set `raise_exc` to False in order to see custom exception handling
@custom_decorator("hello_world_raise", raise_exc=True)
def hello_world_two() -> None:
    print("Hello World")


for i in range(11):
    hello_world()

for i in range(11):
    hello_world_two()
