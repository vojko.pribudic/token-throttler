import asyncio
from typing import Any, Dict, List

from token_throttler import TokenThrottler, TokenThrottlerException
from token_throttler.storage import RuntimeStorage

bucket_config_one: List[Dict[str, Any]] = [
    {
        "replenish_time": 10,
        "max_tokens": 10,
    },
    {
        "replenish_time": 30,
        "max_tokens": 20,
    },
]
bucket_config_two: List[Dict[str, Any]] = [
    {
        "replenish_time": 30,
        "max_tokens": 20,
    },
]
throttler: TokenThrottler = TokenThrottler(1, RuntimeStorage())
throttler.add_from_dict("hello_world", bucket_config_one)


@throttler.enable("hello_world")
async def hello_world() -> None:
    print("Hello World")


for i in range(10):
    asyncio.run(hello_world())

# insert new config and remove old buckets (previously configured)
throttler.add_from_dict("hello_world", bucket_config_two, remove_old_buckets=True)

# bucket with token limit of 10 was removed, exception should not be hit
try:
    asyncio.run(hello_world())
except TokenThrottlerException:
    print(
        f"bucket_one ran out of tokens, retry in: {throttler.wait_time('hello_world')}"
    )
