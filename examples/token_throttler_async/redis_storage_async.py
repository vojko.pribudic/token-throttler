import asyncio

from fakeredis import FakeAsyncRedis

from token_throttler import TokenBucket, TokenThrottlerAsync, TokenThrottlerException
from token_throttler.storage.redis import RedisStorageAsync

# Replace FakeAsyncRedis with Redis instance :)
throttler: TokenThrottlerAsync = TokenThrottlerAsync(
    1, RedisStorageAsync(redis=FakeAsyncRedis(), delimiter="||")
)


@throttler.enable("hello_world")
async def hello_world() -> None:
    print("Hello World")


async def main() -> None:
    await throttler.add_bucket("hello_world", TokenBucket(5, 10))
    await throttler.add_bucket("hello_world", TokenBucket(30, 20))

    for i in range(10):
        await hello_world()

    try:
        await hello_world()
    except TokenThrottlerException:
        print(
            f"bucket_one ran out of tokens, retry in: {await throttler.wait_time('hello_world')}"
        )

    await asyncio.sleep(5)
    await hello_world()


asyncio.run(main())
