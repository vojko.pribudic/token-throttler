from typing import Optional

import pytest
from fastapi import Depends, FastAPI, HTTPException, Request, status
from fastapi.testclient import TestClient
from pydantic import BaseModel

from src.token_throttler import TokenBucket, TokenThrottler
from src.token_throttler.ext.fastapi import FastAPIThrottler
from src.token_throttler.storage import BucketStorage, RuntimeStorage


@pytest.mark.parametrize(
    "cost,storage,replenish_time,max_tokens,exception",
    [
        (
            1,
            RuntimeStorage(),
            10,
            10,
            HTTPException(
                status_code=status.HTTP_429_TOO_MANY_REQUESTS,
                detail="custom exception",
            ),
        ),
        (
            1,
            RuntimeStorage(),
            10,
            10,
            None,
        ),
    ],
)
def test_fastapi_throttle(
    cost: int,
    storage: BucketStorage,
    replenish_time: int,
    max_tokens: int,
    exception: Optional[Exception],
):
    ban_hammer: TokenThrottler = TokenThrottler(cost=cost, storage=storage)

    class User(BaseModel):
        id: int
        name: str

    u1: User = User(id=1, name="Test")
    users: list[User] = [u1]

    def create_buckets() -> None:
        for user in users:
            ban_hammer.add_bucket(
                str(user.id), TokenBucket(replenish_time=10, max_tokens=10)
            )

    def get_auth_user() -> User:
        return u1

    async def get_user_id(_: Request, auth_user: User = Depends(get_auth_user)) -> str:
        return str(auth_user.id)

    app = FastAPI()
    app.add_event_handler("startup", create_buckets)

    @app.get(
        "/throttle",
        dependencies=[
            Depends(FastAPIThrottler(ban_hammer, exc=exception).enable(get_user_id))
        ],
    )
    async def throttle():
        return {"detail": "This is throttled endpoint"}

    with TestClient(app) as client:
        for i in range(max_tokens + 1):
            response = client.get("/throttle")
            assert (
                response.status_code == status.HTTP_200_OK
                if i < max_tokens
                else status.HTTP_429_TOO_MANY_REQUESTS
            )
