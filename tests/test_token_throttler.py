from types import SimpleNamespace
from typing import Any, Dict, List, Union

import pytest

from src.token_throttler import (
    ThrottlerConfig,
    TokenBucket,
    TokenThrottler,
    TokenThrottlerException,
)
from src.token_throttler.storage import RuntimeStorage


def test_token_throttler_get_bucket(throttler: TokenThrottler):
    bucket: Union[TokenBucket, None] = throttler.get_bucket("1", "30")
    assert bucket is not None and bucket.max_tokens == 60
    bucket = throttler.get_bucket("1", "40")
    assert bucket is None


def test_token_throttler_get_all_buckets(throttler: TokenThrottler):
    buckets: Union[Dict[str, TokenBucket], None] = throttler.get_all_buckets("1")
    assert buckets is not None and len(buckets) == 2
    buckets = throttler.get_all_buckets("2")
    assert buckets is None


def test_token_throttler_add_from_dict(throttler: TokenThrottler):
    bucket_config: List[Dict[str, Any]] = [
        {
            "replenish_time": 30,
            "max_tokens": 60,
        },
        {
            "replenish_time": 40,
            "max_tokens": 60,
        },
    ]
    throttler.add_from_dict("2", bucket_config)
    assert len(throttler._storage.buckets["1"]) == 2
    assert len(throttler._storage.buckets["2"]) == 2

    bucket_config_upsert: List[Dict[str, Any]] = [
        {
            "replenish_time": 30,
            "max_tokens": 60,
        },
    ]
    throttler.add_from_dict("2", bucket_config_upsert, remove_old_buckets=True)
    assert len(throttler._storage.buckets["2"]) == 1


def test_token_throttler_remove_bucket(
    throttler: TokenThrottler, bucket_30_60: TokenBucket, bucket_50_100: TokenBucket
):
    assert len(throttler._storage.buckets["1"]) == 2
    throttler.remove_bucket("1", str(bucket_30_60.replenish_time))
    assert len(throttler._storage.buckets["1"]) == 1
    throttler.remove_bucket("2", str(bucket_30_60.replenish_time))
    assert len(throttler._storage.buckets["1"]) == 1
    throttler.remove_bucket("1", "40")
    assert len(throttler._storage.buckets["1"]) == 1
    throttler.remove_bucket("1", str(bucket_50_100.replenish_time))
    assert "1" not in throttler._storage.buckets


def test_token_throttler_remove_all_buckets(throttler: TokenThrottler):
    assert len(throttler._storage.buckets["1"]) == 2
    throttler.remove_all_buckets("2")
    assert len(throttler._storage.buckets["1"]) == 2
    throttler.remove_all_buckets("1")
    assert "1" not in throttler._storage.buckets
    assert throttler.wait_time("1") == 0


def test_token_throttler_consume(
    throttler: TokenThrottler, throttler_config: ThrottlerConfig
) -> None:
    assert len(throttler._storage.buckets["1"]) == 2
    for i in range(59):
        throttler.consume("1")
    assert throttler.consume("1") is True
    assert throttler.consume("1") is False
    assert (
        throttler.wait_time("1") == throttler.get_all_buckets("1")["30"].replenish_time
    )

    throttler._storage.buckets["1"]["30"].last_replenished = 1648756344
    throttler._storage.replenish(throttler._storage.buckets["1"]["30"])
    assert throttler._storage.buckets["1"]["30"].tokens == 60

    with pytest.raises(KeyError):
        assert throttler.consume("2") is True

    throttler_config.set({"IDENTIFIER_FAIL_SAFE": True})
    assert throttler.consume("2") is True and throttler.consume("3") is True

    throttler_config.set({"ENABLE_THREAD_LOCK": True})
    assert throttler.consume("1") is True


def test_token_throttler_validate(
    throttler: TokenThrottler, bucket_30_60: TokenBucket
) -> None:
    with pytest.raises(TypeError):
        throttler.consume(1)  # type: ignore
    with pytest.raises(TypeError):
        _: TokenThrottler = TokenThrottler(cost="test", storage=RuntimeStorage())  # type: ignore
    with pytest.raises(TypeError):
        _: TokenThrottler = TokenThrottler(cost=1, storage=SimpleNamespace())  # type: ignore
    with pytest.raises(TypeError):
        throttler = TokenThrottler(cost=1, storage=RuntimeStorage())
        throttler.add_bucket(identifier=123, bucket=bucket_30_60)  # type: ignore
    with pytest.raises(TypeError):
        throttler = TokenThrottler(cost=1, storage=RuntimeStorage())
        throttler.add_bucket(identifier="1", bucket=SimpleNamespace())  # type: ignore
    with pytest.raises(TypeError):
        throttler = TokenThrottler(cost=1, storage=RuntimeStorage())
        throttler.remove_bucket(identifier="1", bucket_key=1)  # type: ignore
    with pytest.raises(TypeError):

        @throttler.enable(1)  # type: ignore
        def function_handler() -> str:
            return "yes"

        function_handler()

    with pytest.raises(KeyError):
        bucket_config: List[Dict[str, Any]] = [
            {
                "replenish_time": 30,
                "max_tokens_ttt": 60,
            },
        ]
        throttler.add_from_dict("2", bucket_config)

    with pytest.raises(TypeError):
        bucket_config = [
            {
                "replenish_time": 30,
                "max_tokens": 60,
            },
            {
                "replenish_time": 45,
                "max_tokens": "60",
            },
        ]
        throttler.add_from_dict("2", bucket_config)


def test_token_throttler_enable(throttler: TokenThrottler) -> None:
    @throttler.enable("1")
    def function_handler() -> str:
        return "yes"

    for i in range(59):
        function_handler()

    assert throttler.consume("1") is True
    assert throttler.consume("1") is False

    with pytest.raises(TokenThrottlerException):
        function_handler()


@pytest.mark.asyncio
async def test_token_throttler_enable_async(throttler: TokenThrottler) -> None:
    @throttler.enable("1")
    async def function_handler() -> str:
        return "yes"

    for i in range(59):
        await function_handler()

    assert throttler.consume("1") is True
    assert throttler.consume("1") is False

    with pytest.raises(TokenThrottlerException):
        await function_handler()
