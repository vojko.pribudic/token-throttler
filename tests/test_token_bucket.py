import pytest

from src.token_throttler import TokenBucket


def test_token_bucket_consume(bucket_30_60: TokenBucket) -> None:
    for i in range(20):
        bucket_30_60.consume()
    assert bucket_30_60.tokens == 40

    for i in range(39):
        bucket_30_60.consume()

    assert bucket_30_60.consume() is True
    assert bucket_30_60.consume() is False


def test_token_bucket_validate(bucket_30_60: TokenBucket) -> None:
    with pytest.raises(TypeError):
        bucket_30_60.consume("1")  # type: ignore
    with pytest.raises(TypeError):
        _: TokenBucket = TokenBucket(replenish_time="test", max_tokens=20)  # type: ignore
    with pytest.raises(TypeError):
        _: TokenBucket = TokenBucket(replenish_time=123, max_tokens="20")  # type: ignore
