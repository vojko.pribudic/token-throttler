from typing import Generator

import pytest
import pytest_asyncio
from fakeredis import FakeRedis
from fakeredis.aioredis import FakeRedis as FakeRedisAsync

from src.token_throttler import (
    ThrottlerConfig,
    TokenBucket,
    TokenThrottler,
    TokenThrottlerAsync,
)
from src.token_throttler.storage import RuntimeStorage
from src.token_throttler.storage.redis import RedisStorage, RedisStorageAsync


@pytest.fixture(scope="function")
def throttler_config() -> Generator:
    yield ThrottlerConfig(
        {
            "IDENTIFIER_FAIL_SAFE": False,
            "ENABLE_THREAD_LOCK": False,
        }
    )


@pytest.fixture(scope="function")
def bucket_1_1() -> TokenBucket:
    tb: TokenBucket = TokenBucket(replenish_time=1, max_tokens=1)
    tb.cost = 1
    return tb


@pytest.fixture(scope="function")
def bucket_30_60() -> TokenBucket:
    tb: TokenBucket = TokenBucket(replenish_time=30, max_tokens=60)
    tb.cost = 1
    return tb


@pytest.fixture(scope="function")
def bucket_50_100() -> TokenBucket:
    tb: TokenBucket = TokenBucket(replenish_time=50, max_tokens=100)
    tb.cost = 1
    return tb


@pytest.fixture(scope="function")
def throttler(
    bucket_30_60: TokenBucket,
    bucket_50_100: TokenBucket,
    throttler_config: ThrottlerConfig,
) -> TokenThrottler:
    tt: TokenThrottler = TokenThrottler(
        cost=1, storage=RuntimeStorage(), config=throttler_config
    )
    tt.add_bucket("1", bucket_30_60)
    tt.add_bucket("1", bucket_50_100)
    return tt


@pytest.fixture(scope="function")
def throttler_global_config(
    bucket_30_60: TokenBucket,
    bucket_50_100: TokenBucket,
) -> TokenThrottler:
    tt: TokenThrottler = TokenThrottler(cost=1, storage=RuntimeStorage())
    tt.add_bucket("1", bucket_30_60)
    tt.add_bucket("1", bucket_50_100)
    return tt


@pytest.fixture(scope="function")
def throttler_redis(
    bucket_1_1: TokenBucket,
    bucket_30_60: TokenBucket,
    bucket_50_100: TokenBucket,
    throttler_config: ThrottlerConfig,
) -> TokenThrottler:
    tt: TokenThrottler = TokenThrottler(
        cost=1, storage=RedisStorage(FakeRedis(), "||"), config=throttler_config
    )
    tt.add_bucket("1", bucket_30_60)
    tt.add_bucket("1", bucket_50_100)
    tt.add_bucket("0", bucket_1_1)
    return tt


@pytest_asyncio.fixture(scope="function")
async def throttler_redis_async(
    bucket_1_1: TokenBucket,
    bucket_30_60: TokenBucket,
    bucket_50_100: TokenBucket,
    throttler_config: ThrottlerConfig,
) -> TokenThrottlerAsync:
    tt: TokenThrottlerAsync = TokenThrottlerAsync(
        cost=1,
        storage=RedisStorageAsync(FakeRedisAsync(), "||"),
        config=throttler_config,
    )
    await tt.add_bucket("1", bucket_30_60)
    await tt.add_bucket("1", bucket_50_100)
    await tt.add_bucket("0", bucket_1_1)
    return tt
