import datetime
from types import SimpleNamespace
from typing import Any, Dict, List, Union

import pytest
from fakeredis import FakeRedis
from freezegun import freeze_time

from src.token_throttler import (
    ThrottlerConfig,
    TokenBucket,
    TokenThrottler,
    TokenThrottlerException,
)
from src.token_throttler.storage.redis import RedisStorage


def test_token_throttler_redis_get_bucket(throttler_redis: TokenThrottler):
    bucket: Union[TokenBucket, None] = throttler_redis.get_bucket("1", "30")
    assert bucket is not None and bucket.max_tokens == 60
    cache_key: Union[str, None] = throttler_redis._storage.buckets.get("1", {}).get(
        "30", None
    )
    throttler_redis._storage._delete_bucket(cache_key)
    bucket = throttler_redis.get_bucket("1", "30")
    assert bucket is None
    throttler_redis._storage.remove_bucket("1", "50")
    assert bucket is None
    bucket = throttler_redis.get_bucket("1", "40")
    assert bucket is None


def test_token_throttler_redis_get_all_buckets(throttler_redis: TokenThrottler):
    buckets: Union[Dict[str, TokenBucket], None] = throttler_redis.get_all_buckets("1")
    assert buckets is not None and len(buckets) == 2
    buckets = throttler_redis.get_all_buckets("2")
    assert buckets is None


def test_token_throttler_redis_add_from_dict(throttler_redis: TokenThrottler):
    bucket_config: List[Dict[str, Any]] = [
        {
            "replenish_time": 30,
            "max_tokens": 60,
        },
        {
            "replenish_time": 40,
            "max_tokens": 60,
        },
    ]
    throttler_redis.add_from_dict("2", bucket_config)
    assert len(throttler_redis._storage.buckets["1"]) == 2
    assert len(throttler_redis._storage.buckets["2"]) == 2

    bucket_config_upsert: List[Dict[str, Any]] = [
        {
            "replenish_time": 30,
            "max_tokens": 60,
        },
    ]
    throttler_redis.add_from_dict("2", bucket_config_upsert, remove_old_buckets=True)
    assert len(throttler_redis._storage.buckets["2"]) == 1


def test_token_throttler_redis_remove_bucket(
    throttler_redis: TokenThrottler,
    bucket_30_60: TokenBucket,
    bucket_50_100: TokenBucket,
):
    assert len(throttler_redis._storage.buckets["1"]) == 2
    throttler_redis.remove_bucket("1", str(bucket_30_60.replenish_time))
    assert len(throttler_redis._storage.buckets["1"]) == 1
    throttler_redis.remove_bucket("2", str(bucket_30_60.replenish_time))
    assert len(throttler_redis._storage.buckets["1"]) == 1
    throttler_redis.remove_bucket("1", "40")
    assert len(throttler_redis._storage.buckets["1"]) == 1
    throttler_redis.remove_bucket("1", str(bucket_50_100.replenish_time))
    assert "1" not in throttler_redis._storage.buckets


def test_token_throttler_redis_remove_all_buckets(throttler_redis: TokenThrottler):
    assert len(throttler_redis._storage.buckets["1"]) == 2
    throttler_redis.remove_all_buckets("2")
    assert len(throttler_redis._storage.buckets["1"]) == 2
    throttler_redis.remove_all_buckets("1")
    assert "1" not in throttler_redis._storage.buckets
    assert throttler_redis.wait_time("1") == 0


def test_token_throttler_redis_consume(
    throttler_redis: TokenThrottler, throttler_config: ThrottlerConfig
) -> None:
    assert len(throttler_redis._storage.buckets["1"]) == 2
    for i in range(59):
        throttler_redis.consume("1")
    assert throttler_redis.consume("1") is True
    assert throttler_redis.consume("1") is False
    assert (
        throttler_redis.wait_time("1")
        == throttler_redis.get_all_buckets("1")["30"].replenish_time
    )

    with pytest.raises(KeyError):
        assert throttler_redis.consume("2") is True

    throttler_config.set({"ENABLE_THREAD_LOCK": True, "IDENTIFIER_FAIL_SAFE": True})
    assert throttler_redis.consume("2") is True and throttler_redis.consume("3") is True

    with freeze_time(datetime.datetime.now()) as frozen_datetime:
        assert throttler_redis.consume("0") is True
        frozen_datetime.tick(delta=datetime.timedelta(milliseconds=30))
        assert throttler_redis.consume("0") is False
        frozen_datetime.tick(delta=datetime.timedelta(seconds=1))
        assert throttler_redis.consume("0") is True


def test_token_throttler_redis_validate(
    throttler_redis: TokenThrottler, bucket_30_60: TokenBucket
) -> None:
    with pytest.raises(TypeError):
        throttler_redis.consume(1)  # type: ignore
    with pytest.raises(TypeError):
        _: TokenThrottler = TokenThrottler(cost="test", storage=RedisStorage(FakeRedis(), "||"))  # type: ignore
    with pytest.raises(TypeError):
        _: TokenThrottler = TokenThrottler(cost=1, storage=SimpleNamespace())  # type: ignore
    with pytest.raises(TypeError):
        throttler_redis = TokenThrottler(
            cost=1, storage=RedisStorage(FakeRedis(), "||")
        )
        throttler_redis.add_bucket(identifier=123, bucket=bucket_30_60)  # type: ignore
    with pytest.raises(TypeError):
        throttler_redis = TokenThrottler(
            cost=1, storage=RedisStorage(FakeRedis(), "||")
        )
        throttler_redis.add_bucket(identifier="1", bucket=SimpleNamespace())  # type: ignore
    with pytest.raises(TypeError):
        throttler_redis = TokenThrottler(
            cost=1, storage=RedisStorage(FakeRedis(), "||")
        )
        throttler_redis.remove_bucket(identifier="1", bucket_key=1)  # type: ignore
    with pytest.raises(TypeError):

        @throttler_redis.enable(1)  # type: ignore
        def function_handler() -> str:
            return "yes"

        function_handler()

    with pytest.raises(KeyError):
        bucket_config: List[Dict[str, Any]] = [
            {
                "replenish_time": 30,
                "max_tokens_ttt": 60,
            },
        ]
        throttler_redis.add_from_dict("2", bucket_config)

    with pytest.raises(TypeError):
        bucket_config = [
            {
                "replenish_time": 30,
                "max_tokens": 60,
            },
            {
                "replenish_time": 45,
                "max_tokens": "60",
            },
        ]
        throttler_redis.add_from_dict("2", bucket_config)


def test_token_throttler_redis_enable(throttler_redis: TokenThrottler) -> None:
    @throttler_redis.enable("1")
    def function_handler() -> str:
        return "yes"

    for i in range(59):
        function_handler()

    assert throttler_redis.consume("1") is True
    assert throttler_redis.consume("1") is False

    with pytest.raises(TokenThrottlerException):
        function_handler()


@pytest.mark.asyncio
async def test_token_throttler_redis_enable_async(
    throttler_redis: TokenThrottler,
) -> None:
    @throttler_redis.enable("1")
    async def function_handler() -> str:
        return "yes"

    for i in range(59):
        await function_handler()

    assert throttler_redis.consume("1") is True
    assert throttler_redis.consume("1") is False

    with pytest.raises(TokenThrottlerException):
        await function_handler()
