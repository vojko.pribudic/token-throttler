import datetime
from types import SimpleNamespace
from typing import Any, Dict, List, Union

import pytest
from fakeredis.aioredis import FakeRedis
from freezegun import freeze_time

from src.token_throttler import (
    ThrottlerConfig,
    TokenBucket,
    TokenThrottlerAsync,
    TokenThrottlerException,
)
from src.token_throttler.storage.redis import RedisStorageAsync


@pytest.mark.asyncio
async def test_token_throttler_redis_async_get_bucket(
    throttler_redis_async: TokenThrottlerAsync,
):
    bucket: Union[TokenBucket, None] = await throttler_redis_async.get_bucket("1", "30")
    assert bucket is not None and bucket.max_tokens == 60
    cache_key: Union[str, None] = throttler_redis_async._storage.buckets.get(
        "1", {}
    ).get("30", None)
    await throttler_redis_async._storage._delete_bucket(cache_key)
    bucket = await throttler_redis_async.get_bucket("1", "30")
    assert bucket is None
    await throttler_redis_async._storage.remove_bucket("1", "50")
    assert bucket is None
    bucket = await throttler_redis_async.get_bucket("1", "40")
    assert bucket is None


@pytest.mark.asyncio
async def test_token_throttler_redis_async_get_all_buckets(
    throttler_redis_async: TokenThrottlerAsync,
):
    buckets: Union[Dict[str, TokenBucket], None] = (
        await throttler_redis_async.get_all_buckets("1")
    )
    assert buckets is not None and len(buckets) == 2
    buckets = await throttler_redis_async.get_all_buckets("2")
    assert buckets is None


@pytest.mark.asyncio
async def test_token_throttler_redis_async_add_from_dict(
    throttler_redis_async: TokenThrottlerAsync,
):
    bucket_config: List[Dict[str, Any]] = [
        {
            "replenish_time": 30,
            "max_tokens": 60,
        },
        {
            "replenish_time": 40,
            "max_tokens": 60,
        },
    ]
    await throttler_redis_async.add_from_dict("2", bucket_config)
    assert len(throttler_redis_async._storage.buckets["1"]) == 2
    assert len(throttler_redis_async._storage.buckets["2"]) == 2

    bucket_config_upsert: List[Dict[str, Any]] = [
        {
            "replenish_time": 30,
            "max_tokens": 60,
        },
    ]
    await throttler_redis_async.add_from_dict(
        "2", bucket_config_upsert, remove_old_buckets=True
    )
    assert len(throttler_redis_async._storage.buckets["2"]) == 1


@pytest.mark.asyncio
async def test_token_throttler_redis_async_remove_bucket(
    throttler_redis_async: TokenThrottlerAsync,
    bucket_30_60: TokenBucket,
    bucket_50_100: TokenBucket,
):
    assert len(throttler_redis_async._storage.buckets["1"]) == 2
    await throttler_redis_async.remove_bucket("1", str(bucket_30_60.replenish_time))
    assert len(throttler_redis_async._storage.buckets["1"]) == 1
    await throttler_redis_async.remove_bucket("2", str(bucket_30_60.replenish_time))
    assert len(throttler_redis_async._storage.buckets["1"]) == 1
    await throttler_redis_async.remove_bucket("1", "40")
    assert len(throttler_redis_async._storage.buckets["1"]) == 1
    await throttler_redis_async.remove_bucket("1", str(bucket_50_100.replenish_time))
    assert "1" not in throttler_redis_async._storage.buckets


@pytest.mark.asyncio
async def test_token_throttler_redis_async_remove_all_buckets(
    throttler_redis_async: TokenThrottlerAsync,
):
    assert len(throttler_redis_async._storage.buckets["1"]) == 2
    await throttler_redis_async.remove_all_buckets("2")
    assert len(throttler_redis_async._storage.buckets["1"]) == 2
    await throttler_redis_async.remove_all_buckets("1")
    assert "1" not in throttler_redis_async._storage.buckets
    assert await throttler_redis_async.wait_time("1") == 0


@pytest.mark.asyncio
async def test_token_throttler_redis_async_consume(
    throttler_redis_async: TokenThrottlerAsync, throttler_config: ThrottlerConfig
) -> None:
    assert len(throttler_redis_async._storage.buckets["1"]) == 2
    for i in range(59):
        await throttler_redis_async.consume("1")
    assert await throttler_redis_async.consume("1") is True
    assert await throttler_redis_async.consume("1") is False
    assert (
        await throttler_redis_async.wait_time("1")
        == (await throttler_redis_async.get_all_buckets("1"))["30"].replenish_time
    )

    with pytest.raises(KeyError):
        assert await throttler_redis_async.consume("2") is True

    throttler_config.set({"ENABLE_THREAD_LOCK": True, "IDENTIFIER_FAIL_SAFE": True})
    assert (
        await throttler_redis_async.consume("2") is True
        and await throttler_redis_async.consume("3") is True
    )

    with freeze_time(datetime.datetime.now()) as frozen_datetime:
        assert await throttler_redis_async.consume("0") is True
        frozen_datetime.tick(delta=datetime.timedelta(milliseconds=30))
        assert await throttler_redis_async.consume("0") is False
        frozen_datetime.tick(delta=datetime.timedelta(seconds=1))
        assert await throttler_redis_async.consume("0") is True


@pytest.mark.asyncio
async def test_token_throttler_redis_async_validate(
    throttler_redis_async: TokenThrottlerAsync, bucket_30_60: TokenBucket
) -> None:
    with pytest.raises(TypeError):
        await throttler_redis_async.consume(1)  # type: ignore
    with pytest.raises(TypeError):
        _: TokenThrottlerAsync = TokenThrottlerAsync(cost="test", storage=RedisStorageAsync(FakeRedis(), "||"))  # type: ignore
    with pytest.raises(TypeError):
        _: TokenThrottlerAsync = TokenThrottlerAsync(cost=1, storage=SimpleNamespace())  # type: ignore
    with pytest.raises(TypeError):
        throttler_redis_async = TokenThrottlerAsync(
            cost=1, storage=RedisStorageAsync(FakeRedis(), "||")
        )
        await throttler_redis_async.add_bucket(identifier=123, bucket=bucket_30_60)  # type: ignore
    with pytest.raises(TypeError):
        throttler_redis_async = TokenThrottlerAsync(
            cost=1, storage=RedisStorageAsync(FakeRedis(), "||")
        )
        await throttler_redis_async.add_bucket(identifier="1", bucket=SimpleNamespace())  # type: ignore
    with pytest.raises(TypeError):
        throttler_redis_async = TokenThrottlerAsync(
            cost=1, storage=RedisStorageAsync(FakeRedis(), "||")
        )
        await throttler_redis_async.remove_bucket(identifier="1", bucket_key=1)  # type: ignore
    with pytest.raises(TypeError):

        @throttler_redis_async.enable(1)  # type: ignore
        async def function_handler() -> str:
            return "yes"

        await function_handler()

    with pytest.raises(KeyError):
        bucket_config: List[Dict[str, Any]] = [
            {
                "replenish_time": 30,
                "max_tokens_ttt": 60,
            },
        ]
        await throttler_redis_async.add_from_dict("2", bucket_config)

    with pytest.raises(TypeError):
        bucket_config = [
            {
                "replenish_time": 30,
                "max_tokens": 60,
            },
            {
                "replenish_time": 45,
                "max_tokens": "60",
            },
        ]
        await throttler_redis_async.add_from_dict("2", bucket_config)


@pytest.mark.asyncio
async def test_token_throttler_redis_async_enable(
    throttler_redis_async: TokenThrottlerAsync,
) -> None:
    @throttler_redis_async.enable("1")
    async def function_handler() -> str:
        return "yes"

    for i in range(59):
        await function_handler()

    assert await throttler_redis_async.consume("1") is True
    assert await throttler_redis_async.consume("1") is False

    with pytest.raises(TokenThrottlerException):
        await function_handler()
