import pytest

from src.token_throttler import (
    ThrottlerConfig,
    ThrottlerConfigGlobal,
    TokenThrottler,
    default_config,
)


def test_throttler_config(throttler_config: ThrottlerConfig) -> None:
    # Test undefined attribute
    throttler_config._configure({"TEST": 123})
    assert "TEST" not in throttler_config.__dict__.keys()
    # Test wrong type definition
    with pytest.raises(TypeError) as ex:
        throttler_config.set({"IDENTIFIER_FAIL_SAFE": 123})
    assert (
        "Invalid type for configuration parameter `IDENTIFIER_FAIL_SAFE`"
        == str(ex.value)
        and throttler_config.IDENTIFIER_FAIL_SAFE is False
    )
    # Test setting a wrong config type
    with pytest.raises(TypeError) as ex:
        throttler_config.set("test")  # type: ignore
    assert (
        "Invalid configuration input. Expected <class 'dict'>, got <class 'str'>"
        == str(ex.value)
    )
    # Test empty / invalid config
    with pytest.raises(ValueError) as exc:
        throttler_config.set({})
    assert "Invalid configuration provided: {}" == str(exc.value)
    # Test setting a new config value
    throttler_config.set({"IDENTIFIER_FAIL_SAFE": True})
    assert throttler_config.IDENTIFIER_FAIL_SAFE is True


def test_throttler_instance_config(
    throttler: TokenThrottler, throttler_config: ThrottlerConfig
) -> None:
    non_global_config: ThrottlerConfig = ThrottlerConfig({"IDENTIFIER_FAIL_SAFE": True})
    throttler._config = non_global_config
    assert (
        throttler._config.IDENTIFIER_FAIL_SAFE != throttler_config.IDENTIFIER_FAIL_SAFE
    )
    throttler_config.set({"ENABLE_THREAD_LOCK": True})
    assert throttler._config.ENABLE_THREAD_LOCK != throttler_config.ENABLE_THREAD_LOCK


def test_throttler_global_config(throttler_global_config: TokenThrottler) -> None:
    assert isinstance(throttler_global_config._config, ThrottlerConfigGlobal)
    t2: TokenThrottler = TokenThrottler(
        throttler_global_config._cost, throttler_global_config._storage
    )
    with pytest.warns(
        RuntimeWarning,
        match="Modifying attribute 'IDENTIFIER_FAIL_SAFE' of ThrottlerConfigGlobal.",
    ):
        throttler_global_config._config.IDENTIFIER_FAIL_SAFE = True

    assert (
        t2._config.IDENTIFIER_FAIL_SAFE
        == throttler_global_config._config.IDENTIFIER_FAIL_SAFE
        and t2._config.IDENTIFIER_FAIL_SAFE is True
    )
    with pytest.warns(
        RuntimeWarning,
        match="Modifying attribute 'ENABLE_THREAD_LOCK' of ThrottlerConfigGlobal.",
    ):
        default_config.ENABLE_THREAD_LOCK = True
    assert (
        t2._config.IDENTIFIER_FAIL_SAFE
        == throttler_global_config._config.IDENTIFIER_FAIL_SAFE
        == default_config.ENABLE_THREAD_LOCK
    )
    with pytest.warns(
        RuntimeWarning,
        match="Modifying attribute 'IDENTIFIER_FAIL_SAFE' of ThrottlerConfigGlobal.",
    ):
        default_config.set({"IDENTIFIER_FAIL_SAFE": False})
    with pytest.warns(
        RuntimeWarning,
        match="Modifying attribute 'ENABLE_THREAD_LOCK' of ThrottlerConfigGlobal.",
    ):
        default_config.set({"ENABLE_THREAD_LOCK": False})
    assert (
        t2._config.IDENTIFIER_FAIL_SAFE
        == throttler_global_config._config.IDENTIFIER_FAIL_SAFE
        == default_config.ENABLE_THREAD_LOCK
    )
